#!/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.integrate
import csv
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from os import path
import os
from pythonCaseLibs import *
import re
import tikzplotlib
from scipy.optimize import curve_fit
from os.path import isfile, join

caseName=sys.argv[1]

 
def importU(caseName):
	samplePath=path.join(caseName,"postProcessing","UsampleLines")
	dirToCheck=int(max((np.array(os.listdir(samplePath),dtype=float))))
	w_c_2=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["w_c"]
	r=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["breite"]
	onlyfiles = os.listdir(join(samplePath,str(dirToCheck)))
	arr={}
	for i in onlyfiles:
		d=1e-6*float(i.split("_")[1])
		print(d/r)
		if float(d)>w_c_2:
			arr[float(i.split("_")[1])]=np.transpose(np.loadtxt(path.join(samplePath,str(dirToCheck),i), dtype=float, delimiter=',',skiprows=1, usecols=[0, 1,2,3]))	
	return arr

def interpolateUtau(x,caseName, plott=False):
	arr = importU(caseName)
	y=arr[x][0,:]
	h=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["hoehe"]
	
	u=np.linalg.norm(np.transpose(arr[x][1:3,:]),axis=-1)
	
	nu=toolbox.calcNu(caseName)
	yNorm=y*u/nu
	
	yNorm0=yNorm[yNorm<1][-1]
	yNorm1=yNorm[yNorm>1][0]
	
	u0=u[yNorm==yNorm0]
	u1=u[yNorm==yNorm1]	
	
	utau=u0+(1-yNorm0)*(u1-u0)/(yNorm1-yNorm0)
	if plott:
		plt.plot(u,yNorm)
		plt.axhline(1,linestyle='dashed')
		
		plt.axvline(utau,linestyle='dashed')
		plt.xlabel(r'u [$m\cdot s^{-1}$]')
		plt.ylabel(r'$y\cdot\frac{u}{nu}$ [1]')
		plt.title(r'Geschwindigkeitsprofil an der Stelle $x_{rel}=$'+str(1e-6*x/h))
		plt.yscale('log')
		plt.grid()

		plt.savefig('./Bilder/plt_'+str(np.ceil(1e-4*x/h)*1e-2)+caseName.split("/")[-2]+'.png')
		tikzplotlib.save(path.join("./Bilder",caseName,'plt_'+str(np.ceil(1e-4*x/h)*1e-2)+caseName.split("/")[-2]+'.tex'))		
		plt.savefig(path.join("./Bilder",caseName,'plt_'+str(np.ceil(1e-4*x/h)*1e-2)+caseName.split("/")[-2]+'.png'))

		plt.show()
	return utau[0]
	
	
def interpolateOverHeater(x_int,caseName,plott=False):
	arr = importU(caseName)
	
	h=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["hoehe"]

	r=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["breite"]
	
	utau=[interpolateUtau(i,caseName,False) for i in np.sort(np.array(list(arr.keys())))]
	nu=toolbox.calcNu(caseName)
	xrel=(np.sort(np.array(list(arr.keys()))))
	def f(x,a2,a1,a0): 
		return a2*x**2+a1*x+a0
	popt, pcov = curve_fit(f, xrel*1e-6/r, utau)
	utauint=f(x_int,*popt)
	print(utauint)
	if plott:
		
		x2Plot=np.linspace(0,1,100)
		fig, ax=plt.subplots(1,1)
		ax.scatter(1e-6*xrel/r,utau)
		ax.plot(x2Plot,f(x2Plot,*popt))
		ax.axvline(x_int,linestyle='dashed')
		ax.set_xlabel(r'$x_{rel}$ [1]')
		ax.set_ylabel(r'$u_\tau$ [1]')
		ax.set_title(r'$u_\tau$ an der Stelle $x_{rel}=$'+str(x_int))
		ax.set_yscale('log')
		ax.grid()
		ax.invert_xaxis()
		tikzplotlib.save(path.join("./Bilder",caseName,'heaterInterpolation'+caseName.split("/")[-2]+'.tex'))
		plt.savefig(path.join("./Bilder",caseName,'heaterInterpolation'+caseName.split("/")[-2]+'.png'))
		plt.show()
		
	return utau

def plotUProfileAlongAxis(caoseName):
	# using genfromtxt()

	arr = importU(caseName)
	x=arr.keys()
	fig, ax = plt.subplots(constrained_layout=True)
	h=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["hoehe"]
	utau=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["utau"]
	r=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["breite"]
	nu=toolbox.calcNu(caseName)
	ystars=[]
	xstars=[]
	print("hoehe "+str(h))
	for i in x:
#		print(i)
		u=np.linalg.norm(np.transpose(np.array(arr[i])[1:3,:]),axis=1)
		y=np.transpose(np.array(arr[i])[0,:])
		yp=y*u/nu
		ystar=y[yp==yp[np.abs(yp-5)==min(np.abs(yp-5))]]
		ystars.append(min(ystar)/h)
		xstars.append(-0.1*min(u[y==ystar])+1e-6*i/r)
		ax.plot(-0.1*(u)+1e-6*i/r,np.transpose(np.array(arr[i])[0,:])/h)
	print(ystars)
	#ax.plot(np.array(xstars),np.array(ystars))
	ax.set_xlabel('relative Position des Geschwindigkeitsprofils [1]')
	ax.set_ylabel('relative Höhe y [1]')
	ax.set_title('Geschwindigkeitsprofile in äquidistanten Entfernungen in Strömungsrichtung')
	ax.set_xticks(np.arange(0, 1, step=0.25))
	#secax = ax.twiny()
	ax.grid()
	#ax.set_yscale('log')
	ax.legend()
	ax.invert_xaxis()
	tikzplotlib.save(path.join("./Bilder",caseName,'uPltAlongAxis'+caseName.split("/")[-2]+'.tex'))
	plt.savefig(path.join("./Bilder",caseName,'uPltAlongAxis'+caseName.split("/")[-2]+'.png'))
	plt.show()


try:
	os.mkdir(path.join('./Bilder',caseName))
except:
	pass
interpolateOverHeater(0,caseName,True)
plotUProfileAlongAxis(caseName)
interpolateUtau(799.9999999999999,caseName, plott=True)
