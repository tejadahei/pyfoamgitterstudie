#!/usr/bin/python3.6 
from pythonCaseLibs import * 
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
import json
import numpy as np
with open('params.json', 'r') as fp:
    paramDict = json.load(fp)
widthArray=np.array([2e-5, 3e-5, 1.5e-5])
analyseCase.consecutiveRun(widthArray,paramDict,True,1e-6,N=4)
