#!/usr/bin/python 
import numpy as np
import matplotlib.pyplot as plt
import os
import time as Time
from pythonCaseLibs import *
import threading
import math
#dxy= ['case_dxy_2e-05','case_dxy_2.2e-05','case_dxy_2.1e-05','case_dxy_2.3e-05','case_dxy_2.4e-05']
#dxy=['case_dxy_2e-05','case_dxy_2.1e-05', 'case_dxy_2.2e-05', 'case_dxy_2.3e-05',  'case_dxy_2.4e-05', 'case_dxy_2.6000000000000002e-05', 'case_dxy_2.7e-05', 'case_dxy_2.8e-05', 'case_dxy_2.9e-05','case_dxy_3e-05']
dxy=toolbox.listDirectoriesWithPrefix("case")
a=open("cellNoMap").readlines()
cellnos={}
b=[[j.strip() for j in i.split(":")] for i in a]
print(b)
for i in b:
    print(i)
    cellnos[i[0]]=int(i[1])
print(cellnos)
fig,ax = plt.subplots(1,1)
utau=np.zeros(len(dxy))

dT={}
utau={}
fig,ax=plt.subplots(1,1)
#a=ax.scatter(0,0)
for i in cellnos.keys():
    print(i)
    dT[cellnos[i]]=toolbox.parseTemp(i)[-1]
    utau[cellnos[i]]=math.sqrt(toolbox.parseTau(i)[-1])
    ax.scatter(cellnos[i],dT[cellnos[i]])

#a=ax.scatter(list(dT.keys()),dT,label=r'$\Delta T$')
#b=ax.scatter(dxy,utau, label=r'$\sqrt{\tau_w}$')
#print(utau)
ax.set_title(r'$\Delta T\ in \ Abhängigkeit \  von \ der \ Anzahl an  Zellen \ im Rechengebiet$')
ax.set_xlabel(r'Zellenanzahl')
ax.set_ylabel(r'$\Delta$ T [K]')
ax.legend()
#ax.set_ylabel(r'$\sqrt{\tau_w}$')
#fig.canvas.draw()
#fig.canvas.flush_events()

ax.grid()
plt.yscale('linear')
plt.savefig('./Bilder/konvergenzPlt.png', dpi=300)
plt.show()


