#!/usr/bin/python3.6 
from pythonCaseLibs import * 
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
import json
import numpy as np
import os
import os
from pathlib import Path

def list_directories_with_prefix(prefix):
    current_directory = Path.cwd()
    directories_with_prefix = [entry.name for entry in current_directory.iterdir() if entry.is_dir() and entry.name.startswith(prefix)]
    return directories_with_prefix
listOfDirs=list_directories_with_prefix('case_')
analyseCase.rerun(listOfDirs,True,5e-4,N=4)
