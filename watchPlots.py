#!/usr/bin/python 
import numpy as np
import matplotlib.pyplot as plt
import os
import time as Time
import sys
import threading
import pickle
from pythonCaseLibs import * 
import tikzplotlib
from os import path
cases={}


def ThreadFunc(caseName,cases):
	while True:
		Time.sleep(5)
		f=pickle.load(open(caseName+'/PyFoamRunner.buoyantSimpleFoam.analyzed/pickledPlots','rb'))
	
		res_Uz, res_Ux,res_Uy,res_prgh,res_h=toolbox.parseResiduals(caseName)
		dT=toolbox.parseTemp(caseName)
		tau=toolbox.parseTau(caseName)
		cases['a']['res_Ux'][0].set_data(np.array(range(len(np.array(res_Ux)))),np.array(res_Ux))
		cases['a']['res_Uy'][0].set_data(np.array(range(len(np.array(res_Uy)))),np.array(res_Uy))
		cases['a']['res_Uz'][0].set_data(np.array(range(len(np.array(res_Uz)))),np.array(res_Uz))
		cases['a']['res_prgh'][0].set_data(np.array(range(len(np.array(res_prgh)))),np.array(res_prgh))
		cases['a']['res_h'][0].set_data(np.array(range(len(np.array(res_h)))),np.array(res_h))
		cases['a']['dT'][0].set_data(np.array(range(len(np.array(dT)))),np.array(dT))
		cases['a']['tau'][0].set_data(np.array(range(len(np.array(tau)))),np.array(tau))		
		
		cases['ax'][0].relim()
		cases['ax'][0].autoscale_view()
		cases['ax'][1].relim()
		cases['ax'][1].autoscale_view()
		cases['ax'][2].relim()
		cases['ax'][2].autoscale_view()
		cases['fig'].canvas.draw()
		cases['fig'].canvas.flush_events()
#		print(toolbox.currentSlope(np.array(range(len(np.array(res_Uz)))),np.array(res_Uz))[0])
#		print(toolbox.currentSlope(np.array(range(len(np.array(res_Uy)))),np.array(res_Uy))[0])
#		print(toolbox.currentSlope(np.array(range(len(np.array(res_Ux)))),np.array(res_Ux))[0])
#		print(toolbox.currentSlope(np.array(range(len(np.array(res_prgh)))),np.array(res_prgh))[0])
#		print(toolbox.currentSlope(np.array(range(len(np.array(res_h)))),np.array(res_h))[0])
#		print(toolbox.currentSlope(np.array(range(len(np.array(dT)))),np.array(dT))[0])
#		print([toolbox.isConverged(np.array(range(len(res_prgh))),res_prgh,2e-3) ,toolbox.isConverged(np.array(range(len(res_Ux))),res_Ux,2e-3) ,toolbox.isConverged(np.array(range(len(res_Uy))),res_Uy,2e-3),toolbox.isConverged(np.array(range(len(res_Uz))),res_Uz,2e-3),toolbox.isConverged(np.array(range(len(res_h))),res_h,2e-3),toolbox.isConverged(np.array(range(len(res_prgh))),res_prgh,2e-3),toolbox.isConverged(np.array(range(len(tau))),tau,2e-3),toolbox.isConverged(np.array(range(len(dT))),dT[:],2e-3)])
		converged=toolbox.monitorConvergence2(caseName,tol=1e-3)
		if converged:
			print(converged)
		tikzplotlib.save(path.join("./Bilder",caseName,'konvergenz'+caseName.split("/")[-2]+'.tex'))
		plt.savefig(path.join("./Bilder",caseName,'konvergenz'+caseName.split("/")[-2]+'.png'), dpi=300)
threadPlot=[]

try:
	os.mkdir(path.join('./Bilder',caseName))
except:
	pass
for i in sys.argv[1:]:
	tempfig,tempax,tempa=plotResults.plotResults(i)
	cases={			'fig':	tempfig,
					'ax':	tempax,
					'a':	tempa
				}
	x=threading.Thread(target=ThreadFunc,args=[i,cases])
	threadPlot.append(x)
for i in threadPlot:
	i.start()

plt.show()
