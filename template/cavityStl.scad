
w_c=1000;
h_c=200;
theta_c=54.7;
w_h=25;

module MEMS(w_c,h_c,theta_c, w_h,h_b,r_b,scale){
    scale(v=[1,1,1]*scale){
    union(){
        //translate(v=[0,0,h_b/2]){
        //    cylinder(h=h_b,r=r_b, center = true, $fn=100);
        //}
    difference(){ 
        translate(v=[0,0,-h_c/2]){
            rotate(a=180, v=[1,0,0]){
                rotate(a = 45, v = [0, 0, 1])
                    {
                        linear_extrude(height = h_c, center = true, convexity = 10, twist = 0, slices = 30, scale = 1-2*h_c/(tan(theta_c)*w_c), $fn = 100){
                                square([w_c*sqrt(2)/2,w_c*sqrt(2)/2],true);
                            }
                        }
                    }
                }
//         linear_extrude(height = 0.8, center = true, convexity = 10, twist = 0, slices = 1, scale = 1, $fn = 8){
    //         square([w_c,w_h],true);
      //          }
            }
            }
        }
       }
difference(){
MEMS(w_c,h_c, theta_c, w_h, h_b=1000, r_b=1500,scale = 1);
    translate(v=[0,-1000,0]){
            cube(size=[2000,2000,2000],center=true);
        }
    }