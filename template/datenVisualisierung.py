import numpy as np
import matplotlib.pyplot as plt
import os
import time as Time

import threading





def threadFunction(plot,plotMap,fig):
	while True:
		Time.sleep(3)
		res_Uz=np.transpose(np.genfromtxt('./logs/Uz_0',skip_header=1,skip_footer=1,names=True,dtype=None,delimiter='\t',unpack=True,autostrip=True))
		res_Ux=np.transpose(np.genfromtxt('./logs/Ux_0',skip_header=1,skip_footer=1,names=True,dtype=None,delimiter='\t',unpack=True,autostrip=True))
		res_Uy=np.transpose(np.genfromtxt('./logs/Uy_0',skip_header=1,skip_footer=1,names=True,dtype=None,delimiter='\t',unpack=True,autostrip=True))
		res_h=np.transpose(np.genfromtxt('./logs/h_0',skip_header=1,skip_footer=1,names=True,dtype=None,delimiter='\t',unpack=True,autostrip=True))
		res_prgh=np.transpose(np.genfromtxt('./logs/p_rgh_0',skip_header=1,skip_footer=1,names=True,dtype=None,delimiter='\t',unpack=True,autostrip=True))
		tau=np.sqrt(np.genfromtxt('./postProcessing/WSSAvg/'+str(max(np.array(os.listdir('./postProcessing/WSSAvg'),dtype=int)))+'/surfaceFieldValue.dat'))
		TV=(np.genfromtxt('./postProcessing/tempVorne/'+str(max(np.array(os.listdir('./postProcessing/tempVorne'),dtype=int)))+'/volFieldValue.dat'))
		TH=(np.genfromtxt('./postProcessing/tempHinten/'+str(max(np.array(os.listdir('./postProcessing/tempHinten'),dtype=int)))+'/volFieldValue.dat'))
		dT=TV[:,1]-TH[:,1]
		
		plotMap["plt_Uz"][0].set_data(res_Uz[:,0],res_Uz[:,1])
		plotMap["plt_h"][0].set_data(res_h[:,0], res_h[:,1])
		plotMap["plt_Ux"][0].set_data(res_Ux[:,0],res_Ux[:,1])
		plotMap["plt_Uy"][0].set_data(res_Uy[:,0],res_Uy[:,1])
		plotMap["plt_prgh"][0].set_data(res_prgh[:,0], res_prgh[:,1])
		
		
		plotMap["tau"][0].set_data(tau[:,0], tau[:,1])
		plotMap["gradT"][0].set_data(TH[:,0],dT[:])
		
		fig.canvas.draw()
		fig.canvas.flush_events()
		for i in ax:
			i.relim()
			i.autoscale_view()

timeList=[0]


fig,ax = plt.subplots(3,1,figsize=(24,18 ))
os.system('foamLog log')
##    print(data)	  
#    print(T)
res_h=np.transpose(np.genfromtxt('./logs/h_0',skip_header=1,skip_footer=1,names=True,dtype=None,delimiter='\t',unpack=True,autostrip=True)  )

ax[0].set_xlabel('Zeitschritt')
ax[0].set_ylabel('Wert [1]')
plt_h=ax[0].plot(res_h[:,0],res_h[:,1],label='h')
ax[0].legend()
res_Uz=np.transpose(np.genfromtxt('./logs/Uz_0')  )

res_Ux=np.transpose(np.genfromtxt('./logs/Ux_0')) 

res_Uy=np.transpose(np.genfromtxt('./logs/Uy_0'))      
res_prgh=np.transpose(np.genfromtxt('./logs/p_rgh_0')) 
ax[0].set_xlabel('Zeitschritt')
ax[0].set_ylabel('Wert [1]')
ax[0].set_yscale("log")

tau=np.sqrt(np.genfromtxt('./postProcessing/WSSAvg/'+str(max(np.array(os.listdir('./postProcessing/WSSAvg'))))+'/surfaceFieldValue.dat'))
TV=(np.genfromtxt('./postProcessing/tempVorne/'+str(max(np.array(os.listdir('./postProcessing/tempVorne'))))+'/volFieldValue.dat'))
TH=(np.genfromtxt('./postProcessing/tempHinten/'+str(max(np.array(os.listdir('./postProcessing/tempHinten'))))+'/volFieldValue.dat'))
dT=TV[:,1]-TH[:,1]

pltMap={
"plt_Uz":ax[0].plot(res_Uz[:,0],res_Uz[:,1],label='Uz'),
"plt_Ux":ax[0].plot(res_Ux[:,0],res_Ux[:,1],label='Ux'),
"plt_Uy":ax[0].plot(res_Uy[:,0],res_Uy[:,1],label='Uz'),
"plt_prgh":ax[0].plot(res_prgh[:,0],res_prgh[:,1],label='p_rgh'),
"plt_h":ax[0].plot(res_h[:,0],res_h[:,1],label='h'),
"tau":ax[1].plot(tau[:,0],tau[:,1],label='tau'),
"gradT":ax[2].plot(TH[:,0],dT[:],label='T_v-T_H')
}


#print(T)
#tempProbe=data1[1, :].astype(float)
ax[1].set_xlabel('Zeitschritt')
ax[1].set_ylabel('u_tau [m/s]')
ax[1].set_yscale("linear")
ax[1].legend()
fig.tight_layout()
#print(dir(fig))


ax[0].legend()
ax[0].grid()
ax[1].grid()

ax[2].grid()
x = threading.Thread(target=threadFunction, args=(ax,pltMap,fig))
x.start()

plt.show()
x.join()
x.exit()
