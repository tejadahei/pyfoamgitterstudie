import sys
import Vars
import os
def genCase(t,w_c,w_h,h_c,d,w_d,UBetrag,direction,dxy,caseName,theta,dz):
    listArgs= {'t':t,
                'w_c_orig':w_c,
               'h_c':h_c,
               'd':d,
               'w_d':w_d,
               'w_h':w_h,
               'UBetrag':UBetrag,
               'direction':sys.argv[2],
               'dxy':sys.argv[1],
               'sourceCase':'"'+caseName+'"',
               'theta':theta,
               'stopAt': 'endTime'
         }
    for i in listArgs.keys():
        f=open('./constant/variables/'+str(i),'w')
        f.write(str(i)+' '+str(listArgs[i])+';')
genCase(Vars.t,Vars.w_c,Vars.w_h,Vars.h_c,Vars.d,Vars.w_d,Vars.UBetrag,Vars.direction,Vars.dxy,os.getcwd(),Vars.theta,Vars.dz)
