import numpy as np

class stl:
	
	
	
	def __init__(self,solids):
		self.solids=solids
	
	
	class solid:
		def __init__(self,facets,name):
			self.facets=facets
			self.name=name
		class facet:
			def __init__(self,normal,vertices):
				self.normal=normal
				self.vertices=vertices
	def filterByNormal(self,normal):
		element=[]
		notElement=[]
		for i in self.solids:
			for j in i.facets:
				if (j.normal==normal).all():
					element.append(self.solid.facet(j.normal,j.vertices))
				else:
					notElement.append(self.solid.facet(j.normal,j.vertices))
		return {"element":element,"notElement":notElement}
		
	
	def __str__(self):
		
		strval=''
		
		
		for i in self.solids:
			strval+=("solid	"+i.name+"\n")
			for j in i.facets:
				strval+=("	facet normal	"+' '.join(map(str, j.normal))+"\n		outer loop\n")	
				
				for k in j.vertices:
					strval+=("			vertex	"+' '.join(map(str, k))+"\n")	
				strval+=("		endloop\n	endfacet\n")
			strval+=("endsolid "+ i.name +"\n")
		return strval
		
				
def readStlData(path):
	text=open(path).read()
	textSplit=text.split()
	solidDepth=0
	inLoop=0
	inFacet=0
	
	currentSolid=''
	currentNormal=''
	currentVertice=np.array((0,0,0))
	
	stlData=stl([])
	
	for i in range(len(textSplit)):
		if textSplit[i]=='solid':
			solidDepth+=1
			currentSolid=(textSplit[i+1])
			stlData.solids.append(stl.solid([],currentSolid))
		
		if textSplit[i]=='facet':
			inFacet+=1
			currentNormal=np.array(textSplit[i+2:i+5]).astype(float)
			stlData.solids[-1].facets.append(stl.solid.facet(currentNormal,[]))
			
		if textSplit[i]=='vertex':
			stlData.solids[-1].facets[-1].vertices.append(np.array(textSplit[i+1:i+4]).astype(float))
		

			
	return (stlData)
	
	
	
stlFile=readStlData('./constant/triSurface/OpenSCAD_Model_outside.stl')
filtered=stlFile.filterByNormal(np.array((0,0,1)))
filteredStl=stl([stl.solid(filtered["element"],'cavityPatch'),stl.solid(filtered["notElement"],'cavityWall')])
out=open('./constant/triSurface/OpenSCAD_Model_outside.stl','w')
out.write(str(filteredStl))
out.close()
