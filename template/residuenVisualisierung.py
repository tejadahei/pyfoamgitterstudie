import numpy as np
import matplotlib.pyplot as plt
data = np.genfromtxt('./postProcessing/residuals/0/solverInfo.dat',
                     skip_header=1,
                     skip_footer=1,
                     names=True,
                     dtype=None,
                     delimiter='\t',
                     unpack=True,
                     autostrip=True)
                     
                     
#plt.plotfile('./postProcessing/residuals/0/solverInfo.dat', delimiter='\t', cols=(0, 5), names=('col1', 'col2'), marker='o')
 
data=np.array(data)  
timeSteps=data[0, :].astype(float)
res_U=data[5, :].astype(float)
plt.plot(timeSteps,res_U)
#plt.show()                        
