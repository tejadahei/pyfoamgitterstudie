import numpy as np
import matplotlib.pyplot as plt
import os
import time as Time

import threading

global oldslope
oldslope=[]
def currentSlope(x,y):
	x=x[len(x)-100:]
	y=y[len(y)-100:]
	n = np.size(x)
	# mean of x and y vector
	m_x = np.mean(x)
	m_y = np.mean(y) 
	# calculating cross-deviation and deviation about x
	SS_xy = np.sum(y*x) - n*m_y*m_x
	SS_xx = np.sum(x*x) - n*m_x*m_x
 
    # calculating regression coefficients
	b_1 = SS_xy / SS_xx
	b_0 = m_y - b_1*m_x
    
	return b_1, b_0

def convergenceCheck(T,controlValue, trend):
	slope, displacement=currentSlope(T,controlValue)
	print(slope)
	if (abs(slope)<trend):
		if (len(controlValue)>100):
			return True
		
	




def threadFunction():
	convergenceLog=open('./convergenceLog','w')
	convergenceLog.write('#LOG\n')
	while True:
		Time.sleep(3)
		res_Uz=np.genfromtxt('./logs/Uz_0')

		res_Ux=np.genfromtxt('./logs/Ux_0')

		res_Uy=np.genfromtxt('./logs/Uy_0') 
		res_prgh=np.genfromtxt('./logs/p_rgh_0') 
		res_h=np.genfromtxt('./logs/h_0')
		TV=(np.genfromtxt('./postProcessing/tempVorne/'+str(max(np.array(os.listdir('./postProcessing/tempVorne'),dtype=int)))+'/volFieldValue.dat'))
		TH=(np.genfromtxt('./postProcessing/tempHinten/'+str(max(np.array(os.listdir('./postProcessing/tempHinten'),dtype=int)))+'/volFieldValue.dat'))
		dT=TV[:,1]-TH[:,1]
		os.system('foamLog log')
		tau=np.sqrt(np.genfromtxt('./postProcessing/WSSAvg/'+str(max(np.array(os.listdir('./postProcessing/WSSAvg'),dtype=int)))+'/surfaceFieldValue.dat'))

		convergenceCriteria=[convergenceCheck(res_prgh[:,0],res_prgh[:,1],1e-1) ,convergenceCheck(res_Ux[:,0],res_Ux[:,1],1e-3) ,convergenceCheck(res_Uy[:,0],res_Uy[:,1],1e-3),convergenceCheck(res_Uz[:,0],res_Uz[:,1],1e-3),convergenceCheck(res_h[:,0],res_h[:,1],1e-3),convergenceCheck(tau[:,0],tau[:,1],1e-3),convergenceCheck(np.array(range(len(dT))),dT[:],1e-3)]
		if all(convergenceCriteria):
			f=open('./constant/variables/stopAt','w')
			f.write('stopAt    writeNow;')
			log=open('./log','a')
			log.write('Message from Python script:    convergenceCheck was passed!')
			print('Message from Python script:    convergenceCheck was passed!')
			break
		
timeList=[0]
os.system('foamLog log')
x = threading.Thread(target=threadFunction)
x.start()

plt.show()
x.join()
