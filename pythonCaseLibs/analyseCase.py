
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from . import makeMesh,monitors
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
from os import path

def consecutiveRun(arr,params,parallel=False,tol=1e-3,N=4):
		
	for i in arr:
		currentCase=makeMesh.genCase(params, i)
		
		
		if parallel:
			x = threading.Thread(target=monitors.convergenceMonitor,args=[currentCase,tol])
			print(currentCase)
			theSolver = "buoyantSimpleFoam"
			
			makeMesh.bakeMesh(currentCase,True,np=N)
			x.start()
			result = SteadyRunner(args = ["--progress", "--autosense-parallel" , theSolver,"-case",currentCase]).getData()
			x.join()
		else:
			x = threading.Thread(target=monitors.convergenceMonitor,args=[currentCase,tol])
			print(currentCase)
			theSolver = "buoyantSimpleFoam"
			
			makeMesh.bakeMesh(currentCase,False)
			x.start()
			result = SteadyRunner(args = ["--progress" , theSolver,"-case",currentCase]).getData()
			x.join()

def rerun(arr,parallel=False,tol=1e-3,N=4):
		
	for i in arr:
		currentCase=i
		ParsedParameterFile(path.join(currentCase,"system","controlDict"))["startFrom"]="latestTime"		
		
		if parallel:
			x = threading.Thread(target=monitors.convergenceMonitor,args=[currentCase,tol])
			print(currentCase)
			theSolver = "buoyantSimpleFoam"
			
			x.start()
#			makeMesh.bakeMesh(currentCase,True,np=N)
			result = SteadyRunner(args = ["--progress", "--autosense-parallel" , theSolver,"-case",currentCase]).getData()
			x.join()
		else:
			x = threading.Thread(target=monitors.convergenceMonitor,args=[currentCase,tol])
			print(currentCase)
			theSolver = "buoyantSimpleFoam"
			
			x.start()
#			makeMesh.bakeMesh(currentCase,False)
			result = SteadyRunner(args = ["--progress" , theSolver,"-case",currentCase]).getData()
			x.join()

