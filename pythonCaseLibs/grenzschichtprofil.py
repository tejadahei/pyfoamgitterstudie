import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.integrate
import csv
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from os import path


# open the file in the write mode
yPlus=np.linspace(0,300,301)
def uPlusVisc(yPlus):
	return yPlus

def duPlusTrans(yPlus):
	
	kappa=0.4
	APlus=26
	
	D=1-np.exp(-yPlus/APlus)
	quadratTerm=np.square(np.multiply(kappa*yPlus,D))
	return np.divide(2,(1+np.sqrt(1+4*quadratTerm)))

	
def uPlusTrans(yPlus):
	yw=np.linspace(0,yPlus[0],10000)
	kappa=0.4
	APlus=26
	print('integrating from ' + str(yPlus[0]) + ' to ' + str(yPlus[-1]))
	return np.concatenate(([np.trapz(duPlusTrans(yw),yw)],np.trapz(duPlusTrans(yw),yw)+scipy.integrate.cumtrapz(duPlusTrans(yPlus),yPlus)))

def uPlusMom(yPlus):
	kappa=0.4
	CPlus=5.25
	return (1/kappa)*np.log(yPlus)+CPlus


def returnVelocityProfile(yPmax, nSamples):

	ydriest=np.linspace(0,yPmax,nSamples)
	u=uPlusTrans(ydriest)	
	return ydriest,u
def plotVelocityProfile(yPmax,samples):
	yp,up=returnVelocityProfile(yPmax,samples)
	plt.plot(up,yp)

	plt.axhline(5,linestyle='dashed')
	plt.axhline(50,linestyle='dashed')
	plt.title(r'dimensionsloses Geschwindigkeitsprofil')
	plt.xlabel(r'$u^+$')
	plt.xlabel(r'$y^+$')
	plt.yscale('linear')
	plt.grid()
	plt.show()
	
	
def plotRealVelocityProfile(nu,ut,h,samples):
	yPlusMax=ut*h/nu
	yPlus,uPlusProfile=returnVelocityProfile(yPlusMax,samples)
	uProfile=ut*uPlusProfile
	y=yPlus*nu/ut
	plt.plot(y,uProfile)

	plt.axhline(5,linestyle='dashed')
	plt.axhline(50,linestyle='dashed')
	plt.title(r'dimensionsloses Geschwindigkeitsprofil')
	plt.xlabel(r'$u^+$')
	plt.xlabel(r'$y^+$')
	plt.yscale('linear')
	plt.grid()
	plt.show()
	
def genRealVelocityProfile(nu,ut,h,samples,direction,caseName):
	

	dirRad=math.pi*direction/180
	yPlusMax=ut*h/nu
	yPlus,uPlusProfile=returnVelocityProfile(yPlusMax,samples)
	Profile=([np.multiply(yPlus,nu/ut),math.cos(dirRad)*np.multiply(ut,uPlusProfile),math.sin(dirRad)*np.multiply(ut,uPlusProfile), 1e-32*np.ones(np.shape(uPlusProfile))])
	print(Profile)
	np.savetxt(path.join(caseName,"velocityProfile.csv"), np.transpose(Profile), delimiter=",")

def genSampleLines(samples,caseName):
	controlDict = ParsedParameterFile(path.join(caseName,"system","controlDict"),backup = True )
	domainDimensions = ParsedParameterFile(path.join(caseName,"constant","domainDimensions"),backup = True )
	distance=0.5*domainDimensions["breite"]
	h=domainDimensions["hoehe"]
	direction=domainDimensions["directionRad"]
	sampleRange=np.linspace(distance/samples,distance,samples)
	sampleRangexCoords=sampleRange*math.cos(direction)
	sampleRangeyCoords=sampleRange*math.sin(direction)
	sampleDict=controlDict["functions"]["UsampleLines"]["sets"]
	for i in range(len(sampleRange)):
		
		tempStart=[sampleRangexCoords[i],sampleRangeyCoords[i],0]
		
		tempEnd=[sampleRangexCoords[i],sampleRangeyCoords[i],h]
		tempSampleLine={"type":	"face",
															 "axis":	"z",
															 "start":	tempStart,
															 "end":		tempEnd}
		sampleDict.append(("sample_"+str(sampleRange[i]*1e6),tempSampleLine))
	controlDict.writeFile()
