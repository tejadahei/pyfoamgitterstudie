# ! / usr / bin / env python3
import sys
from os import path
import os
import math
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Applications.PrepareCase import PrepareCase
from PyFoam.Applications.CloneCase import CloneCase
from PyFoam.Applications.Decomposer import Decomposer
from PyFoam.Applications.Runner import Runner
from . import toolbox,grenzschichtprofil
params={"d":5e-6,
	"direction":0.0,
	"dz":	50e-7,
	"h_c":	200e-6,
	"w_c":	1000e-6,
	"w_h":	25e-6,
	"theta": 54.7,
	"t": 0.8e-6,
	"yPlus":10
	}
	
	
def genCase(params, dxy):
	
	caseName = 'case_dxy_'+str(dxy)
	
	os.system('rm -r '+caseName)
	print(caseName)
	CloneCase(args=["template",caseName])
	domainDimensions = ParsedParameterFile(path.join(caseName,"constant","domainDimensions"),backup = True )


	domainDimensions["dxy"]=dxy
	domainDimensions["w_c"]=params["w_c"]
	domainDimensions["w_d"]=params["w_d"]
	domainDimensions["t"]=params["t"]
	domainDimensions["w_h"]=params["w_h"]
	domainDimensions["w_c_orig"]=params["w_c"]
	domainDimensions["d"]=params["d"]
	domainDimensions["h_c"]=params["h_c"]
	domainDimensions["theta"]=params["theta"]
	domainDimensions["utau"]=params["utau"]
	domainDimensions.writeFile()
	
	domainDimensions["d_z"]=16*params["t"]
	domainDimensions["directionRad"]=math.pi*params["direction"]/180
	domainDimensions["breite"]=params["w_c"]*domainDimensions["factorY"]
	domainDimensions["laenge"]=params["w_c"]*domainDimensions["factorX"]
	domainDimensions["hoehe"]=params["w_c"]*domainDimensions["factorZ"]
	domainDimensions["dw_c_2"]=params["h_c"]/math.tan(math.pi*params["theta"]/180.0)
	domainDimensions["hoehe"]=domainDimensions["laenge"]*math.sqrt(0.5)*0.5
	domainDimensions["nh"]=abs(int(1-(math.log(1-(domainDimensions["hoehe"]-params["dz"])*(1-domainDimensions["ERz"])/domainDimensions["d_z"]*domainDimensions["ERz"])/math.log(domainDimensions["ERz"])))-2)

	domainDimensions["nxyCavity"]=int(params["w_c"]/domainDimensions["dxy"])
						
	domainDimensions["nzCavity"]=abs(int(1-(math.log(1-domainDimensions["h_c"]*(1-domainDimensions["ERz"])/domainDimensions["d_z"]*domainDimensions["ERz"])/math.log(domainDimensions["ERz"])))-2)	
	domainDimensions["grading_Rechengebiet"]=domainDimensions["ERz"]**domainDimensions["nh"];
	domainDimensions["nxRechenGebiet"]=abs(int(1-(math.log(1-0.25*math.sqrt(0.5)*(domainDimensions["laenge"]-params["w_c"])*(1-domainDimensions["ERxy"])/domainDimensions["dxy"]*domainDimensions["ERxy"])/math.log(domainDimensions["ERxy"])))-2);
	#domainDimensions["nxRechenGebiet"]=abs(int(1-(math.log(1-0.5*(domainDimensions["laenge"]-params["w_c"])*(1-domainDimensions["ERxy"])/domainDimensions["dxy"]*domainDimensions["ERxy"])/math.log(domainDimensions["ERxy"]))));

	domainDimensions["grading_Rechengebietx"]=domainDimensions["ERxy"]**domainDimensions["nxRechenGebiet"];


	domainDimensions["circleThroughXY"]=math.sqrt(0.5)*domainDimensions["laenge"]*0.5
	b=domainDimensions["breite"]-params["w_c"]
	domainDimensions["nxRechenGebiet"]=abs(int(math.log(0.5*(domainDimensions["w_c"]+domainDimensions["laenge"])/domainDimensions["dxy"])/math.log(domainDimensions["ERxy"]))-2);


	domainDimensions["grading_Rechengebiety"]=domainDimensions["ERxy"]**domainDimensions["nyRechenGebiet"];
	domainDimensions["gradingCavityz"]=-domainDimensions["ERz"]**domainDimensions["nzCavity"];
	#calc "$dz/$dxy"
	domainDimensions.writeFile()

	grenzschichtprofil.genRealVelocityProfile(toolbox.calcNu(caseName),params['utau'],domainDimensions["hoehe"],300,0.0,caseName)



    
	#print(toolbox.calcReNr(caseName))
	print(toolbox.calcyPlus(caseName,0.12,params['utau']))
	return caseName

def bakeMesh(caseName,parallel=False,np=4):
	Runner(["blockMesh", "-case",caseName])
	Runner(["extrudeMesh", "-case",caseName])
	Runner(["snappyHexMesh","-overwrite", "-case",caseName])
	grenzschichtprofil.genSampleLines(10,caseName)

	if parallel:
		Decomposer([caseName,np])
