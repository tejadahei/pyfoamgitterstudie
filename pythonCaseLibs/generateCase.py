# ! / usr / bin / env python3
import sys
from os import path
import math
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Applications.PrepareCase import PrepareCase
from PyFoam.Applications.CloneCase import CloneCase
from PyFoam.Applications.Runner import Runner

def generateCase(dxy, utau):
    caseName = "case_dxy={:.1f}_utau={:.2f}".format(dxy,utau)
    print(caseName)
    CloneCase(args=["template",caseName])
    domainDimensions = ParsedParameterFile(path.join(caseName,"system", "blockMeshDict"),backup = True )


    domainDimensions["dxy"]=dxy
    #domainDimensions["UBetrag"]=UBetrag
    domainDimensions["w_c"]=domainDimensions["w_c_orig"] 

    domainDimensions["breite"]=domainDimensions["w_c"]*domainDimensions["faktorY"] 
    
    domainDimensions["laenge"]=domainDimensions["w_c"]*domainDimensions["faktorX"] 
    
    domainDimensions["hoehe"]=domainDimensions["w_c"]*domainDimensions["faktorZ"] 
    grenzschichtprofil.genRealVelocityProfile(nu,ut,h,samples,direction,caseName)
