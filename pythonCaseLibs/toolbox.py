import math
import numpy as np
from os import path
import os
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
import pickle
from . import plotResults
import warnings
from . import grenzschichtprofil
import os
from pathlib import Path
def calcNu (caseName):
	thermophysicalProperties = ParsedParameterFile(path.join("template","constant", "thermophysicalProperties"),backup = True )

	As=thermophysicalProperties["mixture"]["transport"]["As"]
	Ts=thermophysicalProperties["mixture"]["transport"]["Ts"]
	T=293.00
	Rm=8.3144621
	M=thermophysicalProperties["mixture"]["specie"]["molWeight"]*1e-3
	R=Rm/M
	rho=1e5/(R*T)
	mu=As*math.sqrt(T)/(1+Ts/T)
	nu=mu/rho
	return nu

###DEPRECATED
def calcReNr(caseName):
	thermoPhysicalProperties=domainDimensions=ParsedParameterFile(path.join(caseName,"constant","thermophysicalProperties"),backup = False )
	domainDimensions = ParsedParameterFile(path.join(caseName,"constant","domainDimensions"),backup = True )
	L=domainDimensions["w_c"]
	UBetrag=domainDimensions["UBetrag"]
	Re=UBetrag*L/calcNu(caseName)
	return Re
	


	
def parseTemp(caseName):
		TV=(np.genfromtxt('./'+caseName+'/postProcessing/tempVorne/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/tempVorne'),dtype=int)))+'/surfaceFieldValue.dat'))
		TH=(np.genfromtxt('./'+caseName+'/postProcessing/tempHinten/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/tempHinten'),dtype=int)))+'/surfaceFieldValue.dat'))
		
		try:
			TV[:,0]==TH[:,0]
			dT=TH[:,1]-TV[:,1]	
			T=TV[:,0]
		except Exception as e:
				print(e)
		return dT
		
def parseTau(caseName):
		tau=(np.genfromtxt('./'+caseName+'/postProcessing/WSSAvg/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/WSSAvg'),dtype=int)))+'/surfaceFieldValue.dat'))

		return tau[:,1]
		
def parseResiduals(caseName):
	res=(np.genfromtxt('./'+caseName+'/postProcessing/residuals/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/residuals'),dtype=int)))+'/solverInfo.dat'))
	res_Ux=res[:,7]
	res_Uy=res[:,10]
	res_Uz=res[:,13]
	res_prgh=res[:,18]
	res_h=res[:,2]
	
	return res_Ux, res_Uy, res_Uz, res_prgh, res_h
	
def calcyPlus(caseName,yPlus,utau):
	
	domainDimensions = ParsedParameterFile(path.join(caseName,"constant","domainDimensions"),backup = True )
	thermoPhysicalProperties=ParsedParameterFile(path.join(caseName,"constant","thermophysicalProperties"),backup = False )

	y=yPlus*calcNu(caseName)/(utau)
	return y


def currentSlope(x,y):
	x=x[len(x)-100:]
	y=y[len(y)-100:]
	n = np.size(x)
	# mean of x and y vector
	m_x = np.mean(x)
	m_y = np.mean(y) 
	# calculating cross-deviation and deviation about x
	SS_xy = np.sum(y*x) - n*m_y*m_x
	SS_xx = np.sum(x*x) - n*m_x*m_x
 
    # calculating regression coefficients
	b_1 = SS_xy / SS_xx
	b_0 = m_y - b_1*m_x
    
	return b_1, b_0

def isConverged(T,controlValue, trend):

	if (len(controlValue)>100):
		slope, displacement=currentSlope(T,controlValue)
		print(slope)
		if (abs(slope)<trend):
			return True
		else:
			return False
	else:
		return False
			
def monitorConvergence(caseName):
	try:
		
		os.system('foamLog -case'+caseName+'log')
		res_Uz=np.genfromtxt('./'+caseName+'/logs/Uz_0')

		res_Ux=np.genfromtxt('./'+caseName+'/logs/Ux_0')

		res_Uy=np.genfromtxt('./'+caseName+'/logs/Uy_0') 
		res_prgh=np.genfromtxt('./'+caseName+'/logs/p_rgh_0') 
		res_h=np.genfromtxt('./'+caseName+'/logs/h_0')
		TV=(np.genfromtxt('./'+caseName+'/postProcessing/tempVorne/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/tempVorne'),dtype=int)))+'/volFieldValue.dat'))
		TH=(np.genfromtxt('./'+caseName+'/postProcessing/tempHinten/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/tempHinten'),dtype=int)))+'/volFieldValue.dat'))
		dT=TV[:,1]-TH[:,1]
		tau=np.sqrt(np.genfromtxt('./'+caseName+'/postProcessing/WSSAvg/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/WSSAvg'),dtype=int)))+'/surfaceFieldValue.dat'))

	except Exception as e: 
		print(e)
		return False
	else:
		convergenceCriteria=[isConverged(res_prgh[:,0],res_prgh[:,1],tol) ,isConverged(res_Ux[:,0],res_Ux[:,1],tol) ,isConverged(res_Uy[:,0],res_Uy[:,1],tol),isConverged(res_Uz[:,0],res_Uz[:,1],tol),isConverged(res_h[:,0],res_h[:,1],tol),isConverged(tau[:,0],tau[:,1],tol),isConverged(np.array(range(len(dT))),dT[:],tol)]
		return all(convergenceCriteria)
def monitorConvergence2(caseName,log=open('logg','a'),tol=2e-3):
	try:
		
		res_Ux, res_Uy, res_Uz, res_prgh, res_h=parseResiduals(caseName)
		dT=parseTemp(caseName)
		tau=parseTau(caseName)
	except Exception as e:
		print(str(e)) 
		log.write(str(e))
		
		print("nope")
		return False
	else:
		convergenceCriteria=[isConverged(np.array(range(len(res_prgh))),res_prgh,tol) ,isConverged(np.array(range(len(res_Ux))),res_Ux,tol) ,isConverged(np.array(range(len(res_Uy))),res_Uy,tol),isConverged(np.array(range(len(res_Uz))),res_Uz,tol),isConverged(np.array(range(len(res_h))),res_h,tol),isConverged(np.array(range(len(tau))),tau,tol),isConverged(np.array(range(len(dT))),dT,tol)]

		return all(convergenceCriteria)

def listDirectoriesWithPrefix(prefix):
    current_directory = Path.cwd()
    directories_with_prefix = [entry.name for entry in current_directory.iterdir() if entry.is_dir() and entry.name.startswith(prefix)]
    return directories_with_prefix
