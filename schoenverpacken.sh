#!/bin/bash
curDir=$(pwd)
rm -rf $curDir/gitterstudie
mkdir $curDir/gitterstudie
for i in $(ls -d ./case_dxy_*/);
	do
	mkdir $curDir/gitterstudie/$i
	cp -r $curDir/$i/constant $curDir/gitterstudie/$i
	cp -r $curDir/$i/system $curDir/gitterstudie/$i
	cp -r $curDir/$i/0 $curDir/gitterstudie/$i
	reconstructPar -case $curDir/$i -latestTime
	toCopy=$(ls -d $curDir/$i[0-9]* | sort -nr | head -n1)
	echo $toCopy
	cp -r $toCopy $curDir/gitterstudie/$i
	cp -r $curDir/$i/postProcessing $curDir/gitterstudie/$i
	cp -r $curDir/pythonCaseLibs $curDir/gitterstudie/
	cp -r $curDir/*.py $curDir/gitterstudie/
	cp -r $curDir/Bilder $curDir/gitterstudie/
	cp -r $curDir/*.sh $curDir/gitterstudie/
done
#tar -zcvf $curDir/gitterstudie.tar.gz $curDir/gitterstudie
#rm -rf $curDir/gitterstudie
